/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import interfaz.Entrada;
import java.time.LocalTime;

/**
 * La clase Radio es la clase encargada de tener todas las listas que se utilizan en el programa y la información de la radio. 
 * @author guile
 */

public class Radio {
    private String nombre;
    private String descripcion;
    private String dial;
    private String direccion;
    private String telefono;
    private ArrayList redesSociales;

    public ArrayList<Locutor> locutores;
    public ArrayList<Programa> programas;
    public ArrayList<Concurso> concursos;
    public ArrayList<Top5> tops;   
    
    /**
     * Constructor vacio donde se inicializan los atributos 
     */
    public Radio(){
        
        concursos=new ArrayList<>();
        programas=new ArrayList<>();
        tops = new ArrayList<>();
        locutores= new ArrayList<>();
        if(programas.isEmpty()) inicializarPrograma();
        if(concursos.isEmpty())inicializarConcursos();
    }
    /**
     * Informacion neta de la Radio 
     * @param Nombre 
     * @param Descripcion
     * @param Dial
     * @param Direccion
     * @param Telefono
     * @param redesSociales 
     */
    public Radio(String Nombre,String Descripcion, String Dial, String Direccion, String Telefono, ArrayList<Usuario> redesSociales){
        this.nombre=nombre;
        this.descripcion=descripcion;
        this.dial=dial;
        this.direccion=direccion;
        this.redesSociales=redesSociales;
        
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDial() {
        return dial;
    }

    public void setDial(String dial) {
        this.dial = dial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public ArrayList getRedesSociales() {
        return redesSociales;
    }

    public void setRedesSociales(ArrayList redesSociales) {
        this.redesSociales = redesSociales;
    }

    public ArrayList getLocutores() {
        return locutores;
    }

    public void setLocutores(ArrayList locutores) {
        this.locutores = locutores;
    }

    public ArrayList getProgramas() {
        return programas;
    }

    public void setProgramas(ArrayList programas) {
        this.programas = programas;
    }

    public ArrayList getConcursos() {
        return concursos;
    }

    public void setConcursos(ArrayList concursos) {
        this.concursos = concursos;
    }

    public ArrayList getTops() {
        return tops;
    }

    public void setTops(ArrayList tops) {
        this.tops = tops;
    }
    /**
     * 
     * @return retorna lista de concursos 
     */
    public ArrayList<Concurso> getAllConcursos() {
        ArrayList<Concurso> concursos1 = new ArrayList<>();
        concursos.forEach((con) -> {
            concursos1.add(con);
        });
        return concursos1;
    }

    


    /**
     * Agrega objetos de locutor a la lista Locutores
     * @param locutor llama a la lista de locutores inicializada al comienzo de esta clase 
     */
    

    public void agregarLocutor(Locutor locutor){
         locutores.add(locutor);
        
         
    }
    /**
     * Se detalla infomacion neta de la radio 
     * @param r llama a todas los atributos y metodos de la clase Radio
     */
    
    public void InformacionRadio(Radio r){
        ArrayList<Usuario> listaRedRadio=new ArrayList<Usuario>();
        listaRedRadio.add(new Usuario(redsocial.Facebook,"RADIOESPOL"));
        listaRedRadio.add(new Usuario(redsocial.Twitter,"RadioEspol"));
        listaRedRadio.add(new Usuario(redsocial.Instagram,"RadEspol"));
        String nombre="RADIO-ESPOL";
        String descripcion="Proyecto 2S-2018";
        String dial= "109.2";
        String direccion="Alborada";
        String telefono="0956321556";
               
        r.setNombre(nombre);
        r.setDescripcion(descripcion);
        r.setDial(dial);
        r.setTelefono(telefono);
        r.setRedesSociales(listaRedRadio);                
    }
    /**
     * Muestra la informacion de la Radio
     * @param radio llama a todas los atributos y metodos de la clase Radio
     */
      public void MostrarInformacionRadio(Radio radio) { 
                InformacionRadio(radio);
                System.out.println("----------- Información Radio-----------");
                System.out.println(Entrada.alinear("Nombre:"+","+radio.getNombre(), 20));
                System.out.println(Entrada.alinear("Descripcion:"+","+radio.getDescripcion(), 20));
                System.out.println(Entrada.alinear("Dial:"+","+radio.getDial(), 20));
                System.out.println(Entrada.alinear("Telefono:"+","+radio.getTelefono(), 20));
                System.out.println("-----------------------------------------");
                System.out.println(Entrada.alinear("RED SOCIAL:,USUARIO:",20));
                
              
                ArrayList<Usuario> listaradio = radio.getRedesSociales();
                for(Usuario p:listaradio){
                    System.out.println(Entrada.alinear(p.getRed()+","+p.getUsuario(), 20));
                    
             
                }
                System.out.println("-----------------------------------------");
                
                   
    } 

    /**
     * Inicializa 5 locutores
     */

    public void inicializarLocutores(){
        ArrayList<Usuario> listaUsuario1=new ArrayList<Usuario>();//DEBE TENER EL MISMO LOCUTOR verficacion de que el nombre del usuario es igual de locutor muestra sus  redes sociales
        ArrayList<Usuario> listaUsuario2=new ArrayList<>();
        ArrayList<Usuario> listaUsuario3=new ArrayList<>();
        ArrayList<Usuario> listaUsuario4=new ArrayList<>();
        ArrayList<Usuario> listaUsuario5=new ArrayList<>();
      
        listaUsuario1.add(new Usuario(redsocial.Facebook,"RicVillacis"));
        listaUsuario1.add(new Usuario(redsocial.Twitter,"RicVillacis"));
        listaUsuario1.add(new Usuario(redsocial.Instagram,"RicardoV"));
        listaUsuario2.add(new Usuario(redsocial.Facebook,"FelipeO"));
        listaUsuario2.add(new Usuario(redsocial.Facebook,"FelipeO"));
        listaUsuario3.add(new Usuario(redsocial.Facebook,"GuilleAndrade"));
        listaUsuario4.add(new Usuario(redsocial.Facebook,"EdurdoSap"));
        listaUsuario4.add(new Usuario(redsocial.Instagram,"EduSap"));
        listaUsuario5.add(new Usuario(redsocial.Facebook,"NoheM"));
        listaUsuario5.add(new Usuario(redsocial.Instagram,"NoheliaM"));

        
        agregarLocutor(new Locutor("v@hotmail.com", listaUsuario1, "6545694738","Ricardo","Villacis","0991101285"));
        agregarLocutor(new Locutor("f@hotmail.com", listaUsuario2,"6545747821" ,"Felipe","Ochoa","0993673856"));
        agregarLocutor(new Locutor("g@hotmail.com", listaUsuario3,"6545888573" ,"Guillermo","Andrade","0993627848"));
        agregarLocutor(new Locutor("e@hotmail.com", listaUsuario4, "6545922476","Eduardo","Saporiti","09912719874"));
        agregarLocutor(new Locutor("n@hotmail.com", listaUsuario5,"6545023717" ,"Nohelia","Mejia","0991874635"));

    }
    /**
     * Agrega objetos de programa a la lista de Programas
     * @param programa llama a los atributos y metodos de Programa
     */
    public void agregarPrograma(Programa programa){
         programas.add(programa);
    }
    /**
     * Inicializa la lista de programas con 3 objetos de programa
     */
    public void inicializarPrograma(){ 

        inicializarLocutores();
        ArrayList<DiasSemana> listaDias1=new ArrayList<DiasSemana>();
        ArrayList<DiasSemana> listaDias2=new ArrayList<DiasSemana>();
        ArrayList<DiasSemana> listaDias3=new ArrayList<DiasSemana>();
        
        listaDias1.add(DiasSemana.Lunes);listaDias1.add(DiasSemana.Miercoles);listaDias1.add(DiasSemana.Jueves);
        listaDias2.add(DiasSemana.Martes);
        listaDias3.add(DiasSemana.Lunes);listaDias3.add(DiasSemana.Viernes);
        
        ArrayList<Locutor> locutores1=new ArrayList<Locutor>();
        locutores1.add(locutores.get(0));
        locutores1.add(locutores.get(3));

        
        ArrayList<Locutor> locutores2=new ArrayList<Locutor>();
        locutores2.add(locutores.get(1));
        locutores2.add(locutores.get(2));

        
        ArrayList<Locutor> locutores3=new ArrayList<Locutor>();
        locutores3.add(locutores.get(0));
        locutores3.add(locutores.get(4));

            
            
        agregarPrograma(new Programa( "1","programa1","Programa Predeterminado1",listaDias1 , LocalTime.of(11,12),null,locutores1));
        agregarPrograma(new Programa( "2","programa2","Programa Predeterminado2",listaDias2 , LocalTime.of(10,20),null,locutores2));
        agregarPrograma(new Programa( "3","programa3","Programa Predeterminado3",listaDias3 , LocalTime.of(01,12),null,locutores3));


    }
    
    /**
     * 
     * @param tops5 Llama a los atributos y metodos de Top5 
     */
   
 
    public void agregarTop5(Top5 tops5){
    tops.add(tops5);
    }
    



    /**
     * Inicializa la lista de Tops5 con 2 objetos de top5
     */

    public void inicializarTop5() {
        
        ArrayList<Cancion> listaCanciones1=new ArrayList<>();
        listaCanciones1.add(new Cancion("Cancion1","Artista1",1));
        listaCanciones1.add(new Cancion("Cancion2","Artista2",2));
        listaCanciones1.add(new Cancion("Cancion3","Artista3",3));
        listaCanciones1.add(new Cancion("Cancion4","Artista4",4));
        listaCanciones1.add(new Cancion("Cnacion5","Artista5",5));
        agregarTop5(new Top5(LocalDate.of(2018,6,22),listaCanciones1));
        ArrayList<Cancion> listaCanciones2=new ArrayList<>();
        listaCanciones2.add(new Cancion("Cancion1","Artista1",1));
        listaCanciones2.add(new Cancion("Cancion2","Artista2",2));
        listaCanciones2.add(new Cancion("Cancion3","Artista3",3));
        listaCanciones2.add(new Cancion("Cancion4","Artista4",4));
        listaCanciones2.add(new Cancion("Cnacion5","Artista5",5));
        agregarTop5(new Top5(LocalDate.of(2018,8,22),listaCanciones2));
        
   }  
 
    

       

    /**
     * añade objetos concurso a la lista de concursos
     * @param concurso llama a los atributos y metodos de Concurso 
     */

    public void agregarConcurso(Concurso concurso){
        int con = Entrada.compararFechas(concurso.getFechaFin());
        if(con ==0) concurso.setEstado(false);
        concursos.add(concurso);
        } 
    

    /**
     * Inicializa la lista concursos con 2 objetos de Concurso
     */
    public void inicializarConcursos(){
        ArrayList<Participante> participantes = new ArrayList<>();
        participantes.add(new Participante("12345", "Participante1","apellido1","12341234"));
        participantes.add(new Participante("12346", "Participante2","apellido2","22341234"));
        participantes.add(new Participante("12347", "Participante3","apellido3","32341234"));
        ArrayList<Participante> participantes2 = new ArrayList<>();
        participantes2.add(new Participante("12341", "Participante4","apellido4","42341234"));
        participantes2.add(new Participante("12342", "Participante5","apellido5","5341234"));
        participantes2.add(new Participante("12343", "Participante6","apellido6","62341234"));
        
        agregarConcurso(new Concurso( "Concurso1",LocalDate.of(2018,6,18), LocalDate.of(2019,6,19), "Programa1","Premio1",participantes ));
        
        agregarConcurso(new Concurso( "Concurso2", LocalDate.of(2018,6,22), LocalDate.of(2018,6,18), "Programa2","Premio2",participantes2 ));
    }
/**
 * Elimina un concurso de la lista de concursos
 * @param codigo llama al codigo de concursos
 */
    public void eliminarConcurso(String codigo){
        Iterator<Concurso> it = concursos.iterator();
        while(it.hasNext()){
            Concurso conc = it.next();
            if(conc.getCodigo().equalsIgnoreCase(codigo)){
                it.remove();
                System.out.println("Concurso eliminado! ");
            }
        }
        
    }  
}
