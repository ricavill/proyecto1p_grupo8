/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;
/**
 *Clase que permitirá crear objetos para un lista de Canciones que se almacenará en Top5
 * @author guile
 */
public class Cancion {
    private String nombre;
    private String artista;
    private int ubiTop5;
  
    
    public Cancion(){
        
    }
    /***
     * 
     * @param nombre nombre de la cancion
     * @param artista artista de la cancion 
     * @param ubiTop5 ubicacion que tendra en el top5
     */
    public Cancion(String nombre, String artista, int ubiTop5) {
        this.nombre = nombre;
        this.artista = artista;
        this.ubiTop5 = ubiTop5;
    }


    public String getNombre() {
        return nombre;
    }

    public String getArtista() {
        return artista;
    }

    public int getUbiTop5() {
        return ubiTop5;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public void setUbiTop5(int ubiTop5) {
        this.ubiTop5 = ubiTop5;
    }
        
}
    

