/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;
import SistemaRadio.Locutor;
import SistemaRadio.Concurso;
import SistemaRadio.Programa;
import SistemaRadio.Radio;
import SistemaRadio.Top5;
import SistemaRadio.Usuario;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;


/**
 *
 * @author guile
 */
public class Locutor extends Persona {
    private String email;
    private ArrayList<Usuario> redP;

    public Locutor() {
    }

    
    public Locutor(String email, ArrayList<Usuario> redP, String ci, String nombre, String apellidos, String telefono) {
        super(ci, nombre, apellidos, telefono);
        this.email = email;
        this.redP = redP;
    }

    public Locutor(String email, ArrayList<Usuario> redP) {
        this.email = email;
        this.redP = redP;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public ArrayList<Usuario> getRedP() {
        return redP;
    }

    public void setRedP(ArrayList<Usuario> redP) {
        this.redP = redP;
    }

    }
    
    
