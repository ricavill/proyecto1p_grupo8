/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author ricardo
 */
public class Usuario {
    private redsocial red;
    private String Usuario;

    public Usuario() {
    }
    

    public Usuario(redsocial red, String Usuario) {
        this.red = red;
        this.Usuario = Usuario;
    }
    

    public redsocial getRed() {
        return red;
    }

    public void setRed(redsocial red) {
        this.red = red;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    /**
     * Crea usuarios y los guarda en una lista
     * @return devuelve un ArrayList de tipo Usuario
     */
    public  ArrayList<Usuario> CrearUsuario(){
        ArrayList<Usuario> lu=new ArrayList<Usuario>();
        Scanner scan= new Scanner(System.in);
        Scanner scan2= new Scanner(System.in);

        boolean flag2=true;
        while(flag2){
            boolean flag=true;
            while(flag){
                System.out.println("\n\t\tRedes sociales");
                System.out.println("1:Facebook");
                System.out.println("2:Twitter");
                System.out.println("3:Instagram");
                System.out.println("4:Youtube");
                System.out.println("");
                System.out.println("Ingrese opcion(1-4):");
                int num=scan.nextInt();
                if((num==1)||(num==2)||(num==3)||(num==4)){
                    switch(num){
                        case(1):
                            redsocial red1= redsocial.Facebook;
                            System.out.print("Ingrese el usuario: ");
                            String usu1=scan2.nextLine();
                            Usuario us1=new Usuario(red1,usu1);
                            lu.add(us1);
                            flag=false;
                            break;
                        
                        case(2):
                            redsocial red2= redsocial.Twitter;
                            System.out.print("Ingrese el usuario: ");
                            String usu2=scan2.nextLine();
                            Usuario us2=new Usuario(red2,usu2);
                            lu.add(us2);
                            flag=false;
                            break;        
                        
                        case(3):
                            redsocial red3= redsocial.Instagram;
                            System.out.print("Ingrese el usuario: ");
                            String usu3=scan2.nextLine();
                            Usuario us3=new Usuario(red3,usu3);
                            lu.add(us3);
                            flag=false;
                            break;        
                        
                        case(4):
                            redsocial red4= redsocial.Youtube;
                            System.out.print("Ingrese el usuario: ");
                            String usu4=scan2.nextLine();
                            Usuario us4=new Usuario(red4,usu4);
                            lu.add(us4);
                            flag=false;
                            break;        
                    }
                }else{
                    System.out.println("Numero no valido");
                     }
            }
            System.out.print("Pulse(ENTER) para continuar o (N) para dejar de ingresar redes sociales: ");
            String nop=scan2.nextLine();
            if(nop.equals("N")){
                flag2=false;
            }
        }
        return lu;
    }
}