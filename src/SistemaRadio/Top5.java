/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;

import java.time.LocalDate;
import java.util.ArrayList;


/**
 * Clase Top5 donde se encuentran los constructores y metodos getters and setters para poder acceder a sus atributos 
 * @author guile
 */
public class Top5 {
    private ArrayList<Cancion> canciones;
    private LocalDate fecha;
    private ArrayList <Programa> programas;
/**
 * inicializa los arraylist de Cancion y Programa
 */    
    public Top5 () {
        this.canciones = new ArrayList<Cancion>();
        this.programas = new ArrayList<Programa>();
        
    }
 /**
  * 
  * @param fecha String que contendrá la fecha de cada op5 
  * @param canciones Lista que contendrá 5 canciones por cada top
  */   
    public Top5( LocalDate fecha, ArrayList<Cancion> canciones) {
        this.canciones = canciones;
        this.fecha = fecha;
    }   
 /**
  * 
  * @param fecha String que contendrá la fecha de cada op5 
  * @param canciones Lista que contendrá 5 canciones por cada top
  * @param programas Lista de programas  a cual se podrá acceder para consultar los programas  
  */       
    public Top5( LocalDate fecha, ArrayList<Cancion> canciones, ArrayList<Programa> programas) {
        this.canciones = canciones;
        this.fecha = fecha;
        this.programas= programas;
    }
    
/**
 * 
 * @return retorna la lista de canciones
 */
   public ArrayList<Cancion> getCanciones() {
        return canciones;
    }
/**
 * 
 * @param canciones permite asignar una lista de canciones 
 */
    public void setCanciones(ArrayList<Cancion> canciones) {
        this.canciones = canciones;
    }
/**
 * 
 * @return retorna la fecha del top 
 */
    public LocalDate getFecha() {
        return fecha;
    }
/**
 * 
 * @param fecha permite asignar una fecha al top 
 */
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    

    }

   
    
 

    
