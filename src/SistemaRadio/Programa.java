/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author guile
 */
public class Programa {
    private String nombre;
    private String descripcion;
    private ArrayList<DiasSemana> dias;
    private LocalTime horaInicio;
    private LocalTime horaFin;
    private ArrayList<Locutor> locutores;
    private String codigo;
    
    
    public Programa(){
    }    
        
      
   
    public Programa(String codigo, String nombre, String descripcion,ArrayList<DiasSemana> dias, LocalTime horaInicio, LocalTime horaFin, ArrayList<Locutor> locutores) {
        this.codigo=codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.dias = dias;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.locutores = locutores;
    }
   
    /**
     * añade un locutor al arraylist de locutores de Programa
     * @param locutor que se agregara a la listas de locutores.
     */
    public void añadirLocutor(Locutor locutor){
        locutores.add(locutor);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<DiasSemana> getDias() {
        return dias;
    }

    public void setDias(ArrayList<DiasSemana>  dias) {
        this.dias = dias;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public LocalTime getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(LocalTime horaFin) {
        this.horaFin = horaFin;
    }

    public ArrayList<Locutor> getLocutores() {
        return locutores;
    }

    public void setLocutores(ArrayList<Locutor> locutores) {
        this.locutores = locutores;
    }
    
    

}       