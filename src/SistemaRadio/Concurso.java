/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaRadio;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author guile
 */
public class Concurso {
    private String codigo;
    private String nombre;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String programa;
    private String premio;
    private boolean estado= true;
    private ArrayList<Participante> participantes;
    private String ganador="";
    private static int cont = 0;
   
    /**
     * añade un programa al arraylist de programas del concurso
     * @param programa que se agregara a la listas de programas.
     */
    
    public Concurso(){
        participantes = new ArrayList<>();
        
        
    }
    public Concurso( String nombre, LocalDate fechaInicio, LocalDate fechaFin, String programa, String premio, ArrayList<Participante> participantes) {
        codigo = String.valueOf(cont+=1);
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.programa = programa;
        this.premio = premio;
        this.participantes = participantes;
        
        
    }
    
    

    public Concurso( String nombre, LocalDate fechaInicio, LocalDate fechaFin, String programa, String premio) {
        codigo = String.valueOf(cont+=1);
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.programa = programa;
        this.premio = premio;
        participantes = new ArrayList<>();
        
    }
    
    
    public void AñadirParticipante(Participante participante){
        participantes.add(participante);
    }
    
    
    
    public ArrayList<Participante> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(ArrayList<Participante> participantes) {
        this.participantes = participantes;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }
    
    
    
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getPremio() {
        return premio;
    }

    public void setPremio(String premio) {
        this.premio = premio;
    }
    
}
