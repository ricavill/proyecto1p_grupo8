/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import Submenus.*;
import SistemaRadio.*;
import interfaz.Entrada;
import java.text.ParseException;
import java.util.Scanner;

/**
 *
 * @author guile
 */
public class Principal {

    public Radio radio;
    Scanner sc;

    public Principal(Radio radio) {
        this.radio = radio;

        sc = new Scanner(System.in);
    }

    public static void main(String[] args) {
        Radio radio = new Radio();
        Principal ui = new Principal(radio);
        ui.menuPrincipal(radio);

    }

    public void menuPrincipal(Radio radio) {
        SystemaLocutor sistemaLocutor = new SystemaLocutor();
        SystemaPrograma sistemaPrograma = new SystemaPrograma();
        SystemaConcurso sistemaConcurso = new SystemaConcurso();
        SystemaTop5 sistemaTop5 = new SystemaTop5();
        Entrada.bienvenida();
        radio.MostrarInformacionRadio(radio);
        String opcion = "";
        while (!opcion.equals("5")) {
            System.out.println("\n\033[30m                \033[36mMENU PRINCIPAL\033[30m                ");
            opcion = Entrada.showmenu(new String[]{"Administrar locutores", "Administrar programas", "Administrar concursos", "Administrar Top5", "Salir"});
            switch (opcion) {
                case "1":

                    sistemaLocutor.menuLocutor(radio);
                    break;
                case "2":
                    sistemaPrograma.menuPrograma(radio);
                    break;
                case "3":
                    sistemaConcurso.menuConcurso(radio);
                    break;
                case "4":
                    sistemaTop5.menuTop5(radio);
                    break;
                case "5":
                    break;

            }
        }
    }
}
