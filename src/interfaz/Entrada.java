/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;
import java.time.LocalDate;
import java.util.Scanner;

/**
 *
 * @author guile
 */
public class Entrada {
    public static Scanner sc = new Scanner(System.in);
    public static String[] semana={"lunes","martes","miercoles","jueves","viernes","sabado","domingo"};
    public static int[] diasMes= {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    /**
     * Pregunta por pantalla, no verifica nada
     * @param pregunta : la pregunta a la que el usuario respondera
     * @return String la respuesta
     */
    public static String ask(String pregunta) {
        System.out.print(pregunta);
        String retorno = sc.nextLine();
        return retorno;
    }
    
    /**
     * solo muestra por pantalla el menu, no pregunta nada
     * @param opciones : array de las opciones que tendra el menu
     */

    public static void show(String[] opciones){
        int contador = 1; 
        System.out.println("\033[30m╔                                         ╗");
       
        for(String opcion : opciones){
            System.out.println("║ "+contador+")   "+alinear(opcion,35)+"║"); 
            contador++;
        }
        System.out.println("╚                                         ╝");
                            
    }
    
    /**
     * Muestra el menu y pregunta la opcion que desea, corrobora que la opcion estece entre ellas
     * @param opciones : array de las opciones que tendra el menu
     * @return String que pregunta el menu
     */
    public static String showmenu(String[] opciones){
        String op="";
        while(!op.equals(opciones.length)){
            show(opciones);
            op = ask("Ingrese opcion:");
            for (int i = 0;i<=opciones.length;i++){
                if (op.equals(Integer.toString(i))){
                    return op;
                }
            }
        }
        return "";
    }
    
    /**
     * alinea los datos dados
     * @param datos (String) palabras separadas por comas
     * @param espacios (int) cantidad de espacios entra palabras
     * @return un String con las palabras separadas x una cantidad de espacios dado
     */
    public static String alinear(String datos, int espacios){
        String[] palabras =datos.split(",");
        String texListo="";
         for(String palabra :palabras){
            String es="";
            for (int i=0;i<(espacios-palabra.length());i++) es+=" ";
            texListo+= palabra+es; 
        }
        return texListo;
    }
     
    public static LocalDate pedirFecha(String frase) {
        int mes=15;
        int dia=60;
        int anio=9999;
        LocalDate fecha=null;
        do{
            System.out.print(frase); 
            String[] dat= sc.nextLine().split("-");
            if (dat.length==3 && dat[0].length()<=2 && dat[1].length()<=2&& dat[2].length()==4){                
                if (dat[0].length()==1 && Character.isDigit(dat[0].charAt(0)) ) dia= Integer.parseInt(dat[0]);
                if (dat[0].length()==2&&Character.isDigit(dat[0].charAt(0))&&Character.isDigit(dat[0].charAt(1)) )dia= Integer.parseInt(dat[0]);
                if (dat[1].length()==1 && Character.isDigit(dat[1].charAt(0)) ) mes= Integer.parseInt(dat[1]);
                if (dat[1].length()==2&&Character.isDigit(dat[1].charAt(0))&&Character.isDigit(dat[1].charAt(1)) )mes= Integer.parseInt(dat[1]);
                if (Character.isDigit(dat[2].charAt(0))&&Character.isDigit(dat[2].charAt(1))&& Character.isDigit(dat[2].charAt(2))&&Character.isDigit(dat[2].charAt(3)) )anio= Integer.parseInt(dat[2]);
                if(mes<13&&anio<2100&&anio>=LocalDate.now().getYear()){
                    if (anio%4==0 && mes==2 ) { 
                        if (dia<=(diasMes[mes-1]+1))fecha=LocalDate.of(anio,mes,dia);
                    }
                    else if ( dia<=(diasMes[mes-1]))  fecha=LocalDate.of(anio,mes,dia);
                }
                else Error("Fecha Invalida");
            }else Error("Fecha Invalida");
        }while (fecha==null);
        return fecha;
    }
    public static void Error(String frase){
        
        System.out.println("\u001B[31m *******! "+frase+" !*******\u001B[34m");
        
    }
    
    /**
     * 
     * @param fecha Ingresa una fecha cualquiera
     * @return retorna int 1 si la fecha ingresada es mayor a la actual y retorna int 0 caso contrario
     */
    public static int compararFechas(LocalDate fecha){
        int num = 0;
        LocalDate fechaNow = LocalDate.now();
        if(fecha.getYear()>fechaNow.getYear()) num =1;
        if(fecha.getYear()<fechaNow.getYear()) num =0;
        if(fecha.getYear()==fechaNow.getYear()){
            if(fecha.getDayOfYear()>fechaNow.getDayOfYear()) num =1;
        }return num;
    }
    
    /**
     * 
     * @param fecha1 Ingresa una fecha a comparar
     * @param fecha2 Ingresa otra fecha a comparar
     * @return retorna int 1 si la fecha1 ingresada es mayor a la fecha2 y retorna int 0 caso contrario
     */
    public static int compararFechas2(LocalDate fecha1, LocalDate fecha2){
        int num = 0;        
        if(fecha1.getYear()>fecha2.getYear()) num =1;
        if(fecha1.getYear()<fecha2.getYear()) num =0;
        if(fecha1.getYear()==fecha2.getYear()){
            if(fecha1.getDayOfYear()>fecha2.getDayOfYear()) num =1;
        }return num;
    }
    
    /**
     * Imprime un logo del programa
     */
    static void bienvenida() {
        System.out.println();
        System.out.println("\u001B[34m        ◘W◘W◘W◘W◘W◘W◘W◘W◘W◘");
        System.out.println("           R a d i o  E s p o l ");
        System.out.println("\u001B[34m        ◘W◘W◘W◘W◘W◘W◘W◘W◘W◘");
        System.out.println("\u001B[34m               Bienvenido");
   
    }
    

}
