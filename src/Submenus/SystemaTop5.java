/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Submenus;
import SistemaRadio.*;
import interfaz.Entrada;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 *En esta clase se registran los Top5 que puedan tener los programas de la Radio
 * @author guile
 * 
 * 
 */


public class SystemaTop5 {
  SystemaPrograma sp = new SystemaPrograma(); //dado que se puede asignar un programa para cada top se instancia la clase SystemaPrograma
    /**
     * Presenta el menu principal del Top5  
     * @param radio llama a radio ya que allí se encuentran todas las listas que se usan en los metodos de Top5
     */   
    public void menuTop5(Radio radio) {
        String opcion ="";
        while(!opcion.equals("3")){
            if(radio.getTops().isEmpty()) radio.inicializarTop5();
            opcion=Entrada.showmenu(new String[] {"Registrar Top5","ConsultarTop5","Volver al menu principal"});
            switch(opcion){
                case "1":
                    RegistrarTop5(radio);
                    break;
                case "2":
                    ConsultarTop5(radio);
                    break;
                case "3":
                    break;
                    
            }
        }
    }
       
   /**
    * 
    * @param r llama a radio ya que se debe asignar un programa al top5 y la lista de programas se encuentra en la clase Radio, 
    * y en la lista de Tops5 que también se encuentra en radio es en donde se almacerá la información obtenida en este metodo.
    * 
    */    
   public void RegistrarTop5(Radio r) {
        
        System.out.print("-------------REGISTRO DE TOP5-------------\n");
        Scanner  leer = new Scanner(System.in);
        String salida="S";  
            
        while(!"N".equals(salida)){    
            LocalDate fechaInicio= Entrada.pedirFecha("\nIngrese fecha(dia-mes-año)"); //fecha del top
            int i = 0;
            while(i<4){
                ArrayList<Cancion> listaCanciones=new ArrayList<>(); //lista de canciones creada dentro del ciclo para que solo mantenga las cinco canciones requiridas 
                for ( int c=0; c<5; c++ ){                
                String nombre=Entrada.ask("\nIngrese el nombre la cancion en top"+(i+1)+":");
                String artista=Entrada.ask("Ingrese el artista:");
                Cancion cancion=new Cancion(nombre,artista,i+1); //creacion de objeto
                listaCanciones.add(cancion); //adición del objeto a lista de canciones
                System.out.println("\nCancion agregada");        
                i+=1;
                }
                System.out.print("Ha finalizado las 5 canciones\n");
                boolean ver=true;
                while(ver){
                    sp.MostrarPrograma(r); //llamada al método MostrarPrograma de SytemaPograma
                    String resp =Entrada.ask("\nIndique el programa de donde se lo elige:");
                    ArrayList<Programa> listaProgramas = r.getProgramas(); //Se instancia la lista de Programas
                    for (Programa p: listaProgramas){ //se recorre la lista para verificar si existe el programa ee donde se lo elige
                    if ((p.getNombre().equals(resp))==false){
                        ver=true;
                    }else{
                        ver=false;
                    }
                    }   
                }       
            System.out.println("\nRegistro top5 existoso");    
            r.agregarTop5(new Top5(fechaInicio,listaCanciones));
            }
            System.out.print("\nPresione(ENTER) para continuar o (N) si ya no desea seguir continuando");
            salida=leer.next();
        }
    }
    
/**
 * ConsultarTop5 presenta por pantalla la fecha y la lista de canciones de cada top que se encuentre registrado en la lista de Tops5
 * @param r  Llama a la clase Radio ya que allí se encuentra lista de Tops5 que se va a presentar mediante este método. 
 */        
    public void ConsultarTop5(Radio r){
        ArrayList<Top5> listatop = new ArrayList<>();
        listatop = r.getTops();
        System.out.println("--------- CONSULTAR TOP 5---------");
        for(int t=listatop.size()-1; t>=0 ;t--) {
            System.out.println("Fecha:"+listatop.get(t).getFecha());
            for(int i=0; i<listatop.get(t).getCanciones().size(); i++){
                     System.out.printf("%-20s %-20s %-20s\n", listatop.get(t).getCanciones().get(i).getNombre(),
                                                                listatop.get(t).getCanciones().get(i).getArtista(),
                                                              "TOP"+listatop.get(t).getCanciones().get(i).getUbiTop5());
            }
        }
    }  

}

