/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Submenus;
import java.util.Random;
import interfaz.Entrada;
import SistemaRadio.*;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author guile
 */
public class SystemaConcurso {
    
    
    /**
     * Metodo que muestra un menu con las opciones de administrar concurso. 
     * @param radio recibe la variable instanciada de Radio
     */  
    public void menuConcurso(Radio radio){                
        String opcion ="";
        while(!opcion.equals("6")){ 
            mostrarConcursos(radio);
            opcion=Entrada.showmenu(new String[] {"Mostrar Concurso","Agregar Concurso","Eliminar Concurso","Inscribir Participantes en Concurso","Asignar Ganador de Concurso","Volver al menu principal"});
            
            switch(opcion){                
                case "1":
                    mostrarConcurso(radio);                   
                    break;
                case "2":                    
                    agregarConcurso(radio);
                    break;
                case "3":                    
                    verificarCodigo2(radio);  
                    break;
                case "4":
                    inscribirParticipante(radio);
                    break;
                case "5":
                    asignarGanador(radio);
                    break;
                case "6":
                    break;
                    
            }
        }
    }
    
     
     /**
      * Metodo que muestra todos los concursos ingresados en el sistema.
      * @param radio recibe la variable instanciada de Radio
      */        
     private void mostrarConcursos(Radio radio){
         System.out.println("------------------- CONSULTAR CONCURSOS-------------------");
        ArrayList<Concurso> concursos = radio.getAllConcursos();
        System.out.println("");
        System.out.printf("%-20s %-20s %-20s","Codigo","Nombre","Estado");
        System.out.println("");

        for(Concurso concurso: concursos){
            
            if(concurso.isEstado()==true){
                String estado="Activo";
                System.out.printf("%-20s %-20s %-20s",concurso.getCodigo(),concurso.getNombre(),estado);
                System.out.println("");
            }
            if(concurso.isEstado()==false){
                String estado ="Inactivo";
                System.out.printf("%-20s %-20s %-20s",concurso.getCodigo(),concurso.getNombre(),estado);
                System.out.println("");
            }
        }
    }
     
    /**
     * Metodo que muestra todos los datos de un solo concurso.
     * @param radio recibe la variable instanciada de Radio
     */ 
    private void mostrarConcurso(Radio radio){
       
        String codConcurso = verificarCodigo(radio);        
        System.out.println(" ");
        Concurso concurso = buscarConcurso(codConcurso, radio);
        if(concurso.isEstado()==true) {
            System.out.println("nombre: "+concurso.getNombre()+"\nFecha de inicio: "+concurso.getFechaInicio()+"\nFecha de finalizacion: "+concurso.getFechaFin()+"\nPrograma: "+concurso.getPrograma());   
            ArrayList<Participante> participantes;
            participantes = concurso.getParticipantes();
            System.out.println("PARTICIPANTES:");
            for(Participante participante: participantes){
                System.out.println(participante.getNombre());                
            }
        }
        if(concurso.isEstado()==false){
            System.out.println("nombre: "+concurso.getNombre()+"\nFecha de inicio: "+concurso.getFechaInicio()+"\nFecha de finalizacion: "+concurso.getFechaFin()+"\nPrograma: "+concurso.getPrograma()+"\nGanador: "+concurso.getGanador());   
            ArrayList<Participante> participantes;
            participantes = concurso.getParticipantes();
            System.out.println("PARTICIPANTES:");
            for(Participante participante: participantes){
                System.out.println(participante.getNombre());
        
            }
        }   
    }
    
    /**
     * Metodo que pide el codigo de un concurso y verifica si existe.
     * @param radio recibe la variable instanciada de Radio
     * @return devuelve un String con el codigo del concurso verificado.
     */
    public String verificarCodigo(Radio radio){
        Concurso con= null;
        String codConcurso=" ";
        do{
            codConcurso = Entrada.ask("Introduzca el codigo del concurso: ");
            for(Concurso concurso: radio.getAllConcursos()){
                if(concurso.getCodigo().equalsIgnoreCase(codConcurso)) con=concurso;
            }if(con==null) System.out.println(" codigo no existe! ");
        }while(con==null);
        return codConcurso;
    }
    
    /**
     * Metodo que busca un concurso mediante su codigo.
     * @param codigo recibe el codigo del concurso que quiere buscar.
     * @param radio recibe la variable instanciada de Radio
     * @return devuelve el objeto concurso que encontro.
     */
    public Concurso buscarConcurso(String codigo, Radio radio){                
        Concurso con= null;        
        for(Concurso concurso: radio.concursos ){
             if(concurso.getCodigo().equalsIgnoreCase(codigo)) con=concurso;                               
        }return con;        
    }
    
    
    /**
     * Metodo que agrega un concurso en el sistema.
     * @param radio recibe la variable instanciada de Radio
     */
    
    public void agregarConcurso(Radio radio){
        
        String nombre = Entrada.ask("Ingrese nombre : ");
        int con = 0;
        int con2 =0;
        LocalDate fechaInicio = null;
        LocalDate fechaFin = null;
        while(con2==0){
            
            do{
                fechaInicio= Entrada.pedirFecha("Ingrese fecha Inicio (dia-mes-año) : ");
                int num = Entrada.compararFechas(fechaInicio);
                if(num==0) System.out.println("La fecha ya paso! ");
                else con =1;
            }while(con==0); 
            do{
                fechaFin = Entrada.pedirFecha("Ingrese fecha Fin (dia-mes-año) : "); 
                int num = Entrada.compararFechas(fechaFin);
                if(num==0) System.out.println("La fecha ya paso! ");
                else con =1;
            }while(con==0);                                                                                     
            if(Entrada.compararFechas2(fechaInicio, fechaFin)==1) System.out.println("La  fecha de inicio es mayor a la fecha de fin!!");
            else con2=1;
        }
        String programa = elegirPrograma(radio);
        String premio = Entrada.ask("Ingrese el premio : ");
        radio.agregarConcurso(new Concurso(nombre,fechaInicio,fechaFin,programa, premio)); 
        System.out.println("Se ha agregado el concurso correctamente! ");
    }
    
    
    /**
     * Metodo que muestra los programas que existen y pide el codigo de uno.
     * @param radio recibe la variable instanciada de Radio
     * @return devuelve un String con el codigo del programa que el usuario eligio.
     */    
    public String elegirPrograma(Radio radio){ 
        System.out.println(" ");
        System.out.println(Entrada.alinear("PROGRAMA:,CODIGO:", 20));
        for(Programa programa: radio.programas ){
            
            System.out.println(Entrada.alinear(programa.getNombre()+","+programa.getCodigo(), 20));
            
        }
        System.out.println(" ");
        String programa="1";
        
        while(programa=="1"){
            String codigo = Entrada.ask("Ingrese codigo del programa : ");                                                            
            if(codigo.equalsIgnoreCase("")) programa = ""; 
            else {
                for(Programa p : radio.programas){
                    if(p.getCodigo().equalsIgnoreCase(codigo)){
                        programa = p.getNombre() ;
                        
                    }
                }
            } 
        }return programa;  

    }
    
    
    /**
     * Metodo que pide un codigo de concurso y lo valida para a su vez eliminar el concurso.
     * @param radio recibe la variable instanciada de Radio
     */
    public void verificarCodigo2(Radio radio){
        System.out.println(" ");
        ArrayList<Concurso> concursos = radio.getAllConcursos();
        boolean ex = true;
        do{
        String codigo = Entrada.ask("Introduzca codigo : ");
        for(Concurso con: concursos){
                if(con.getCodigo().equalsIgnoreCase(codigo)){
                    
                    ex=false;
                }
            }
        if(ex==false) radio.eliminarConcurso(codigo);
        if(ex==true) System.out.println("Codigo no existe!");
        
        }while(ex);                                           
    }
    
    
    /**
     * Metodo que pide los datos de un participante para guardarlos en un concurso.
     * @param radio recibe la variable instanciada de Radio
     */
    public void inscribirParticipante(Radio radio){
        System.out.println("\n-----------------Concursos activos------------------");
        System.out.println("");
        System.out.printf("%-20s %-20s","Nombre del concurso","Codigo");
        System.out.println("");

        for(Concurso conc : radio.concursos ){
            if(conc.isEstado()==true){
                System.out.println("");
                System.out.printf("%-20s %-20s",conc.getNombre(),conc.getCodigo());
                System.out.println("");
            }
        }
        System.out.println("----------------------------------------------------");
        
        
        boolean val = true;
        while(val){           
            int e=0;
            String cedula = Entrada.ask("\nIngrese cedula: ");
            String nombre = Entrada.ask("Ingrese nombres: ");
            String apellido = Entrada.ask("Ingrese apellidos: ");
            String telefono = Entrada.ask("Ingrese telefono: ");
            Concurso concurso = obtenerConcursoActivo(radio,"Introduzca codigo del concurso al que quiere inscribir: ");
            for(Participante par : concurso.getParticipantes()){                
                if(cedula.equalsIgnoreCase(par.getCi())){
                    System.out.println("El participante ya existe! ");
                    e=1;
                }                
            }
            if(e!=1){
                concurso.AñadirParticipante(new Participante(cedula,nombre,apellido,telefono));
                System.out.println("El participante fue añadido con exito! ");
                System.out.println(" ");
                val = false;
            }
        }
       
    }
    
    
    /**
     * Metodo que obtiene un concurso de los que se encuentran inactivos.
     * @param radio recibe la variable instanciada de Radio
     * @param frase recibe la frase de solicitud del codigo del concurso.
     * @return devuelve un objeto de Concurso.
     */
     public Concurso obtenerConcursoInactivo(Radio radio,String frase){
        
        boolean est = true;
        Concurso concurso = null;              
        while(est){
            System.out.println("----------------------------------------------------------------");
            String codigo = Entrada.ask(frase);
            for(Concurso con :radio.concursos){
                if(con.isEstado()==false){
                    if(con.getCodigo().equalsIgnoreCase(codigo)){
                        concurso  = con;
                        est = false;
                    }
                }                
            }
            if(est) System.out.println("Codigo no valido");
        }return concurso;                       
    }
    
     
    /**
     * Metodo que obtiene un concurso de los que se encuentran activos.
     * @param radio recibe la variable instanciada de Radio
     * @param frase recibe la frase de solicitud del codigo del concurso.
     * @return devuelve un objeto de Concurso.
     */ 
    public Concurso obtenerConcursoActivo(Radio radio,String frase){
        
        boolean est = true;
        Concurso concurso = null;              
        while(est){
            System.out.println("----------------------------------------------------------------");
            String codigo = Entrada.ask(frase);
            for(Concurso con :radio.concursos){
                if(con.isEstado()==true){
                    if(con.getCodigo().equalsIgnoreCase(codigo)){
                        concurso  = con;
                        est = false;
                    }
                }                
            }
            if(est) System.out.println("Codigo no valido");
        }return concurso;                       
    }
    
    
    /**
     * Metodo que muestra las opciones para asignar un ganador.
     * @param radio recibe la variable instanciada de Radio
     */
    public void asignarGanador(Radio radio){
        String opcion ="";
        while(!opcion.equals("3")){             
            opcion=Entrada.showmenu(new String[] {"Asignar ganador aleatorio","Asignar ganador manual","Volver al menu principal"});            
            switch(opcion){                
                case "1":
                    asignarGanadorAleatorio(radio);                    
                    break;
                case "2":                    
                    asignarGanadorManual(radio);
                    break;
                
                case "3":
                    break;
                    
            }
        }
    }
    
    
    /**
     * Metodo que asigna un ganador aleatorio a un concurso en especifico.
     * @param radio recibe la variable instanciada de Radio
     */
    public void asignarGanadorAleatorio(Radio radio){
        System.out.println("\n-------------------Concursos Inactivos---------------------\n");
        System.out.printf("%-20s %-20s","Nombre del concurso","Codigo");
        System.out.println("");

        for(Concurso conc : radio.concursos ){
            if(conc.isEstado()==false){
                System.out.printf("%-20s %-20s",conc.getNombre(),conc.getCodigo());
                System.out.println("");
            }
        }
        Concurso concurso = obtenerConcursoInactivo(radio,"Introduzca el concurso que asignara un ganador ");
        ArrayList<String> nombresParticipantes = new ArrayList<>();
        for(Participante par: concurso.getParticipantes()){
            nombresParticipantes.add(par.getNombre());
        }
        if(!(nombresParticipantes.isEmpty())){
            int num =(int) (Math.random()*(nombresParticipantes.size()-1));
            concurso.setGanador(nombresParticipantes.get(num));
            
            System.out.println("Se le ha asignado un ganador con exito!");
            System.out.println("---------------------------------------------------------------");
            System.out.println("\t\t\tPARTICIPANTE GANADOR: ");
            for(Participante par: concurso.getParticipantes()){
                if(nombresParticipantes.get(num).equalsIgnoreCase(par.getNombre())) System.out.println("\nNombre:"+par.getNombre()+"\nApellido:"+par.getApellidos()+"\nTelefono:"+par.getTelefono());
                System.out.println("----------------------------------------------------------------");
            }
        }
        else{
            System.out.println("----------------------------------------------------------------");
            System.out.println("No existen participantes inscritos!");
            System.out.println("----------------------------------------------------------------");
            //menuConcurso(radio);
        }
        
    }
    
    
    /**
     * Metodo que muestra los participantes de un concurso para que el usuario elija a un ganador manuealmente.
     * @param radio recibe la variable instanciada de Radio
     */
    public void asignarGanadorManual(Radio radio){
        System.out.println("\n------------------Concursos Inactivos-------------------\n");
        System.out.printf("%-20s %-20s","Nombre del concurso","Codigo");
        System.out.println("");

        for(Concurso conc : radio.concursos ){
            if(conc.isEstado()==false){
                System.out.printf("%-20s %-20s",conc.getNombre(),conc.getCodigo());
                System.out.println("");
            }
        }
        Concurso concurso = obtenerConcursoInactivo(radio,"Introduzca el concurso que asignara un ganador ");
        if(!(concurso.getParticipantes().isEmpty())){
            System.out.printf("%-20s %-20s %-20s","Codigo","Nombre","Apellido");
            System.out.println("\n-----------------------PARTICIPANTES---------------------------\n");
            System.out.println("");
            for(Participante par: concurso.getParticipantes()){
                System.out.printf("%-20s %-20s %-20s",par.getCi(),par.getNombre(),par.getApellidos());
                System.out.println("");
            }
            Participante participante = obtenerParticipante(radio,concurso);
            concurso.setGanador(participante.getNombre());
            System.out.println("----------------------------------------------------------------");
            System.out.println("Se le ha asignado un ganador con exito!");
            System.out.println("----------------------------------------------------------------");
            System.out.println("\t\t PARTICIPANTE GANADOR:  ");
            System.out.println("\nNombre: "+participante.getNombre()+"\nApellido: "+participante.getApellidos()+"\nTelefono: "+participante.getTelefono());
            System.out.println("----------------------------------------------------------------");
        }else System.out.println("El concurso no tiene participantes! ");
        
        
    }
    
    
    /**
     * Metodo que te pide el codigo de un participante y lo busca en el concurso.
     * @param radio recibe la variable instanciada de Radio
     * @param concurso recibe un objeto de tipo Concurso.
     * @return devuelve un participante que se obtiene del concurso.
     */
    public Participante obtenerParticipante(Radio radio, Concurso concurso){
        Participante participante = null;
        while(participante==null){
            String codigo = Entrada.ask("Introduzca el codigo del participante: ");
            for(Participante par: concurso.getParticipantes()){
                if(par.getCi().equalsIgnoreCase(codigo)) participante = par;
            }
            if(participante== null) System.out.println("Codigo incorrecto! ");
        }
        return participante;
    }
    
}