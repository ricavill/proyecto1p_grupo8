/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Submenus;
import java.util.ArrayList;
import SistemaRadio.*;
import interfaz.Entrada;
import java.time.LocalTime;
import java.util.Scanner;
/**
 *
 * @author guile
 */

public class SystemaPrograma {
    Programa programa = new Programa();
    Scanner scan=new Scanner(System.in);
    
    /**
     * Presenta el menu principal del Programa   
     * @param radio
     */   
    
    public void menuPrograma(Radio radio){
        
        String opcion ="";
        while(!opcion.equals("4")){
            MostrarPrograma(radio);//Se muestran a los programas
            opcion=Entrada.showmenu(new String[] {"Agregar Programa","Suspender Programa","Editar Programa","Volver al menu principal"});
            
            switch(opcion){
                case "1":
                    AgregarPrograma(radio); //se ejecuta a el metodo agregar programa que mete programas a la lista programas de la radio
                    break;
                
                case "2":
                    System.out.print("Ingrese el codigo del programa: ");
                    String codigo1=scan.nextLine();                            
                    SuspenderPrograma(radio.programas,codigo1);// Se pide el codigo del programa y luego se llama suspender programa para suspender el programa pedido
                    break;
    
                case "3":
                    System.out.print("Ingrese el codigo del programa: ");
                    String codigo2=scan.nextLine();
                    EditarPrograma(radio.programas,radio.locutores,codigo2,radio);//Se pide el codigo del programa para editar el programa pedido
                    break;
               
                case "4":
                    break;
                    
            }
        }  
    }
          
     /**
     * Metodo que muestra todos los programas que existen en la lista programas de "r"(radio).
     * @param r recibe la variable instanciada de Radio.
     */        
    
    public void MostrarPrograma(Radio r){
        ArrayList<Programa> listaProgramas = r.getProgramas();//se define una lista que va a ser igual que las lista de programas que esta en radio
        


        System.out.println("------------------- CONSULTAR PROGRAMAS-------------------");
        System.out.printf("%-20s %-20s %-20s","Codigo","Nombre","Locutores");


        System.out.println("");
        for(Programa p:listaProgramas) {
            ArrayList<String> nloc=new ArrayList<>();//se añade lista para meter a los locutores de cada programa(se reinicia por cada programa)
            p.getLocutores().forEach((x) -> {
                //se itera la lista de programas definida antes para luego llamar a iterar a la lista de los locutores de ella
                nloc.add(x.getNombre());//se añade a la lista los locutores para poder mostrar en el print de abajo la lista de locutores de cada programa
            });
            if(p.getHoraFin()==null){
                System.out.printf("%-20s %-20s %-20s",p.getCodigo(),p.getNombre(),nloc);
                System.out.println("");  
            }
        }
            
}
     /**
     * Metodo que agrega a un programa a la lista de programas del parametro "r".
     * @param r recibe la variable instanciada de Radio.
     */
    public  void AgregarPrograma(Radio r){
        Scanner entrada= new Scanner(System.in);
        Scanner entrada2= new Scanner(System.in);
        int cod= r.programas.size()+1;
        SystemaLocutor lm=new SystemaLocutor();
        ArrayList<Locutor> locrs=new ArrayList<>();
           
        String cod1=Integer.toString(cod);
        
        System.out.print("Ingrese el nombre: ");
        String nom=entrada.nextLine();
        System.out.print("Ingrese la descripción: ");
        String des=entrada.nextLine();
        ArrayList<DiasSemana> dias= DiasSemana.AgregarDia();
        System.out.print("Ingrese hora inicio: ");
        System.out.print("Hora: ");
        int n1=entrada.nextInt();
        System.out.print("Minutos:");
        int n2=entrada.nextInt();
        LocalTime ini=LocalTime.of(n1, n2);
        System.out.println("Lista de locutores: ");
        boolean flag=true;
        while(flag){
            int p=0;
            lm.MostrarLocutores(r);
            System.out.println("Ingrese la cedula del locutor: ");
            String ced=entrada2.nextLine();
            ArrayList<Locutor>loct=r.getLocutores();
            for (Locutor x:loct){
                if(x.getCi().equals(ced)){
                    locrs.add(x);
                    System.out.println("Se añadio el locutor");
                    p=1;
                }
            }
            if(p==0){
                System.out.println("No se encontro el locutor");
            }
            System.out.println("Si no desea agregar locutor escriba -N-");
            String np=entrada2.nextLine();
            if(np.equals("N")){
                flag=false;
            }       
        }
        Programa prog1=new Programa(cod1,nom,des,dias,ini,null,locrs);
        r.agregarPrograma(prog1);
        System.out.println("");
    }
    
    /**
     * Metodo que suspende un programa dandole una fecha de fin al programa.
     * @param pop recibe una lista que se va a ser iterada e igualada al codigo del programa. 
     * @param codigo  recibe un string que va a definir cual es el programa elegido.
     */
    
    public  void SuspenderPrograma(ArrayList<Programa> pop,String codigo){
        int n1=0;
        for(Programa x: pop){
            if(x.getCodigo().equals(codigo)){
                LocalTime ff=x.getHoraInicio();
                x.setHoraFin(ff);
                System.out.println("");
                n1=1;
                System.out.println("Se ha suspendido el programa");
            }
        }
        if(n1==0){
            System.out.println("No se encontro el programa");
        }
    }
    
     /**
     * Metodo que edita el programa de la lista programas de radio que tenga el mismo codigo que el parametro codigo.
     * @param radio recibe la variable instanciada de Radio.
     * @param loct
     * @param codigo
     * @param pop recibe una lista
     */
    public  void EditarPrograma(ArrayList<Programa> pop,ArrayList<Locutor>loct,String codigo,Radio radio){
        String opcion ="";
        int n1=0;
        for (Programa w:pop){
            if (w.getCodigo().equals(codigo)){
        while(!opcion.equals("6")){
            opcion=Entrada.showmenu(new String[] {"Nombre","Descripcion","Dias","Fecha Inicio","Cambiar locutores","Salir"});
            switch(opcion){
                case"1":
                    IngresoNombre(pop,codigo);
                    break;
                case"2":
                    IngresoDescripcion(pop,codigo); 
                    break;                
                case"3":
                    IngresoDias(pop,codigo); 
                    break;
                case"4":
                    IngresoFecha(pop,codigo); 
                    break;
                case"5":
                    CambiarLocutor(pop,codigo,loct,radio); 
                    break;
                
                case"6":
                    break;
                    
        }
            n1=1;
    }

    }  
                        }
        if(n1==0){
            System.out.println("No se encontro el programa");
        }
    System.out.println("");
    }
     /**
     * Metodo para ingresar nuevo nombre al programa.
     * @param pop recibe una lista de programas para poder mediante el parametro codigo encontrar el programa deseado
     * @param codigo recibe codigo para comparar listas de la lista pop
     */
    public void IngresoNombre(ArrayList<Programa> pop,String codigo){
        Scanner scan1=new Scanner(System.in);
                for(Programa x: pop){
                    if(x.getCodigo().equals(codigo)){
                        System.out.print("Ingrese nuevo nombre: ");
                        String nombre2=scan1.nextLine();
                        x.setNombre(nombre2);
                        System.out.println("Nombre editado");
                        
                    }    
                }
            }
     /**
     * Metodo para ingresar nueva descripcion del programa.
     * @param pop recibe una lista de programas para poder mediante el parametro codigo encontrar el programa deseado
     * @param codigo recibe codigo para comparar listas de la lista pop
     */
    
    public void IngresoDescripcion(ArrayList<Programa> pop,String codigo){
        Scanner scan1=new Scanner(System.in);

        for(Programa x: pop){
                    if(x.getCodigo().equals(codigo)){
                        System.out.print("Ingrese nueva descripción: ");
                        String des2=scan1.nextLine();
                        x.setDescripcion(des2);
                        System.out.println("Descripción editada");
                    }    
                }
        }
     /**
     * Metodo para ingresar nueva lista de dias.
     * @param pop recibe una lista de programas para poder mediante el parametro codigo encontrar el programa deseado
     * @param codigo recibe codigo para comparar listas de la lista pop
     */
        public void IngresoDias(ArrayList<Programa> pop,String codigo){    
                    ArrayList<DiasSemana> dias = null;

        for(Programa x: pop){
                    if(x.getCodigo().equals(codigo)){
                        dias= DiasSemana.AgregarDia();
                    }
                    x.setDias(dias);
                }
                System.out.println("Dias del programa editado");
                
    }
     /**
     * Metodo para ingresar nuevas fecha de inicio.
     * @param pop recibe una lista de programas para poder mediante el parametro codigo encontrar el programa deseado
     * @param codigo recibe codigo para comparar listas de la lista pop
     */

        public void IngresoFecha(ArrayList<Programa> pop,String codigo){    
        Scanner leer=new Scanner(System.in); 
        for(Programa x: pop){
                    if(x.getCodigo().equals(codigo)){
                        System.out.print("Ingrese hora inicio: ");
                        System.out.print("Hora: ");
                        int n1=leer.nextInt();
                        System.out.print("Minutos:");
                        int n2=leer.nextInt();
                        LocalTime ini1=LocalTime.of(n1,n2);
                        x.setHoraInicio(ini1);
                        x.setHoraFin(null);
                        System.out.println("Fecha de inicio editada");
                    }    
                }  
}
     /**
     * Metodo para cambiar la lista locutor del programa.
     * @param pop recibe una lista de programas para poder mediante el parametro codigo encontrar el programa deseado
     * @param codigo recibe codigo para comparar listas de la lista pop
     * @param loct recibe una lista de locutores de radio para saber usar solo los locutores que estan dentro de radio
     * @param radio recibe la variable instanciada de Radio.
     */
    public void CambiarLocutor(ArrayList<Programa> pop,String codigo,ArrayList<Locutor>loct,Radio radio){   
        SystemaLocutor lm= new SystemaLocutor();
        ArrayList<Locutor> locrs= new ArrayList<>();
        Scanner scan1=new Scanner(System.in); 

        for(Programa p: pop){
                if(p.getCodigo().equals(codigo)){                
                    System.out.println("Lista de locutores: ");
                    boolean flag3=true;
                    while(flag3){
                        lm.MostrarLocutores(radio);
                        int po=0;
                        System.out.println("Ingrese la cedula del locutor: ");
                        String ced=scan1.nextLine();
                        for(Locutor x:loct){
                            if(x.getCi().equals(ced)){
                                locrs.add(x);
                                po=1;
                            }
                        }
                        if(po==0){
                            System.out.println("No se encontro el locutor");
                        }
                        
                        System.out.println("Locutor agregado");
                        System.out.println("");
                        System.out.println("Si no desea agregar locutor escriba -N-");
                        System.out.println("");
                        String np=scan1.nextLine();
                        if(np.equals("N")){
                            flag3=false;
                        }
                    }          
                    p.setLocutores(locrs);
            }   
        }
    } 
}

