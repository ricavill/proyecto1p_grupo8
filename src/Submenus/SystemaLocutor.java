/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Submenus;
import SistemaRadio.*;
import SistemaRadio.Locutor;
import interfaz.Entrada;
import java.util.Scanner;

import java.util.ArrayList;
import java.util.Iterator;
/**
 *
 * @author guile
 */
public class SystemaLocutor {
    Scanner scan= new Scanner(System.in);

    /**
     * Presenta el menu principal del Locutor   
     */   
    public void menuLocutor(Radio radio){
        Locutor loc11=new Locutor();
        
        String opcion ="";
        while(!opcion.equals("4")){
        MostrarLocutores(radio);
        System.out.println("\n\033[30m                \033[36mMENU LOCUTOR\033[30m                ");

            opcion=Entrada.showmenu(new String[] {"Mostrar Locutor","Agregar Locutor","Eliminar Locutor","Volver al menu principal"});
            
            switch(opcion){
                case "1":
                    String cedula = Entrada.ask("Ingrese cedula: ");
                    Mostrarlocutor(radio.locutores, cedula);
                    break;
                case "2":
                    AgregarLocutor(radio);
                    break;
                case "3":
                    System.out.println("Ingrese cedula:");
                    String cedula1=scan.nextLine();                    
                    EliminarLocutor(radio.locutores,radio.programas,cedula1);
                    break;
              
                case "4":
                    break;
                    
            }
        }
    }
    /**
     * Metodo que muestra todos los locutores dentro de la radio.
     * @param r recibe la variable instanciada de Radio.
     */
    public void MostrarLocutores(Radio r){
        ArrayList<Locutor> listalocutores = r.getLocutores();

        System.out.println("\n---------------- CONSULTAR LOCUTORES ---------------\n");
        System.out.printf("%-20s %-20s %-20s","Cedula","Nombre","Apellido");
        System.out.println("\n");
        
        for(Locutor l:listalocutores) {
           
            System.out.printf("%-20s %-20s %-20s",l.getCi(),l.getNombre(),l.getApellidos());
            System.out.println("");
            
        }
           
        
}
     /**
     * Muestra al locutor que tenga el mismo numero de cedula.
     * @param loc recibe una lista que sirva para comparar el locutor con el parametro ci.
     * @param ci recibe un String con el que se van a comparar la cedula de los locutores de la lista loc de radio
     */
   public   void Mostrarlocutor(ArrayList<Locutor> loc,String ci){
        int cont=0;
        if(loc.size()==0){
            System.out.println("No hay locutores");
        }
        else{
            for(Locutor x: loc){
            if(x.getCi().equals(ci)){
                System.out.println("--- Información---");
                System.out.println("Nombre:"+ x.getNombre());
                System.out.println("Apellido:"+ x.getApellidos());
                System.out.println("C.I.:"+ x.getCi());
                System.out.println("Telefono:"+ x.getTelefono());
                System.out.println("email:"+x.getEmail());
                System.out.println("Redes sociales y usuario:");
                for(Usuario p:x.getRedP() ){
                    System.out.printf(p.getRed()+": "+p.getUsuario());
                    System.out.println("");
                    
                }

            }
            else{
                cont++;
            }
            }
            if(cont==loc.size()){
               System.out.println("No se encontro el locutor");
            }
        
        }
    }
     /**
     * Metodo que agrega un locutor a la lista locutores de radio.
     * @param r recibe la variable instanciada de Radio.
     */
    public void AgregarLocutor(Radio r){
        Usuario us2=new Usuario();
        String cid = Entrada.ask("Ingrese C.I: ");
        int n=0;
        for (Locutor p:r.locutores){
            if(p.getCi().equals(cid)){
                System.out.println("El locutor ya se encuentra");
                n=1;
                break;
            }
        }
        if (n==0){
        String nombre1= Entrada.ask("Ingrese nombre: ");
        String apellido1= Entrada.ask("Ingrese el apellido: ");
        String telf= Entrada.ask("Ingrese el telefono: ");
        String ema= Entrada.ask("Ingrese el email: ");
        ArrayList<Usuario> red=us2.CrearUsuario();
        Locutor loc1=new Locutor(ema,red,cid,nombre1,apellido1,telf);
        

        
        r.agregarLocutor(loc1);    
        System.out.println("Se ha agregado el locutor");
        System.out.println("");
        }
    }    
        
    /**
     * Metodo que elimina a un locutor que tenga la misma cedula que lo ingresado de la lista de locutores de radio y ademas lo elimina de los programas a los que fue asignado.
     * @param loc recibe una lista que contiene locutores que se compararan con el parametro ci.
     * @param proc recibe la lista de programas de radio.
     */
    public  void EliminarLocutor(ArrayList<Locutor> loc,ArrayList<Programa>proc,String ci){
        Iterator<Locutor> it=loc.iterator();
        int u=0;
        while(it.hasNext()){
            Locutor loce=it.next();
            if(loce.getCi().equals(ci)){
                it.remove();
                System.out.println("Locutor eliminado");
                u=1;
                System.out.println("");
            }
        }
        if(u==0){
           System.out.println("No se encontro al locutor");
        }
        for(Programa p:proc){
            for(Locutor pi:p.getLocutores()){
                if(pi.getCi().equals(ci)){
                    p.setLocutores(null);
                }
            
            }
        
        
        }
    
    }    

}

